# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 22:06:41 2017

@author: Xeobo
"""


from keras.models import Sequential
from keras.layers import Activation
from keras.layers import Convolution1D
from keras.layers import MaxPooling1D
from keras.layers import Flatten
from keras.layers import Dense
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

def windows(data, size):
    start = 0
    while start < data.count():
        yield start, start + size
        start += int(size / 2)
        
def segment_signal(data,window_size = 90):
    segments = np.empty((0,window_size,3))
    labels = np.empty((0))
    for (start, end) in windows(data["timestamp"], window_size):
        print("start: " + str(start) + ", end" + str(end))
        x = data["x-axis"][start:end]
        y = data["y-axis"][start:end]
        z = data["z-axis"][start:end]
        if(len(dataset["timestamp"][start:end]) == window_size):
            segments = np.vstack([segments,np.dstack([x,y,z])])
            labels = np.append(labels,stats.mode(data["activity"][start:end])[0][0])
    return segments, labels

# Load data
column_names = ['user-id','activity','timestamp', 'x-axis', 'y-axis', 'z-axis']
dataset = pd.read_csv('WISDM_ar_v1.1_raw.txt', names = column_names, header = None, comment = ";")

dataset.replace(np.inf, np.nan)
dataset.dropna()


np.any(np.isnan(dataset['z-axis']))

print(dataset['z-axis'][343419])

# Feature scaling
scX = StandardScaler()
dataset['x-axis'] = scX.fit_transform(dataset['x-axis'].values.reshape(-1,1))
scY = StandardScaler()
dataset['y-axis'] = scY.fit_transform(dataset['y-axis'].values.reshape(-1,1))
scZ = StandardScaler()
dataset['z-axis'] = scZ.fit_transform(dataset['z-axis'].values.reshape(-1,1))


print(dataset)

# Segment data
segments, labels = segment_signal(dataset)

print(scX.scale_)
print(scX.mean_)


# trasform categorical labels
labelencoder = LabelEncoder()
new_labels = labelencoder.fit_transform(labels)
onehotencoder = OneHotEncoder(categorical_features = [0])
new_labels = onehotencoder.fit_transform(new_labels.reshape(-1,1)).toarray()

# Reshape segments
reshaped_segments = segments.reshape(len(segments),90, 3)

print(reshaped_segments)

# Split data sets
X_train, X_test, y_train, y_test \
    = train_test_split(reshaped_segments, new_labels, test_size = 0.3, random_state = 0)

classifier = Sequential()
# Step 1 - Convolution
classifier.add(Convolution1D(filters = 60, kernel_size = 60, input_shape = (90, 3), activation = 'relu'))

# Step 2 - Pooling
classifier.add(MaxPooling1D(pool_size = 20, stride = 2))

# Adding a second convolutional layer
classifier.add(Convolution1D(filters = 6, kernel_size = 6, activation = 'relu'))

# Step 3 - Flattening
classifier.add(Flatten())

# Step 4 - Full connection
classifier.add(Dense(units = 10000, activation = 'relu'))
classifier.add(Dense(units = 6, activation = 'sigmoid'))
classifier.add(Activation('softmax'))

classifier.compile(optimizer = "adam", loss = "categorical_crossentropy", metrics = ["accuracy"])

classifier.fit( X_train, y_train, batch_size = 10, epochs = 5, validation_data = (X_test, y_test))

# Save model to files
classifier.save_weights("har_model_save")
json_string = classifier.to_json()
text_file = open("har_model_json.json","w")
text_file.write(json_string)
text_file.close()

# Save scale parameters
scale_parameters_file = open("har_scale_parameters.csv","w")
scale_parameters_file.write(str(scX.mean_[0]) + "," + str(scX.scale_[0]) + "\n" )
scale_parameters_file.write(str(scY.mean_[0]) + "," + str(scY.scale_[0]) + "\n" )
scale_parameters_file.write(str(scZ.mean_[0]) + "," + str(scZ.scale_[0]) + "\n" )
scale_parameters_file.close()


# Save model as pb
import tensorflow as tf
import os
import os.path as osp
from keras import backend as K
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io

K.set_learning_phase(0)
num_output = 6
pred = [None]*num_output
pred_node_names = [None]*num_output
for i in range(num_output):
    pred_node_names[i] = "output_node"+str(i)
    pred[i] = tf.identity(classifier.output[i], name=pred_node_names[i])

pred_node_names = "output_node"

output_graph_name = 'constant_graph_weights.pb'
output_fld = "."
sess = K.get_session()
constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(), pred_node_names)
graph_io.write_graph(constant_graph, output_fld, output_graph_name, as_text=False)
print('saved the constant graph (ready for inference) at: ', osp.join(output_fld, output_graph_name))



from tensorflow.python.tools import freeze_graph

MODEL_NAME = 'har'

input_graph_path = 'checkpoint/' + MODEL_NAME+'.pbtxt'
checkpoint_path = './checkpoint/' +MODEL_NAME+'.ckpt'
restore_op_name = "save/restore_all"
filename_tensor_name = "save/Const:0"
output_frozen_graph_name = 'frozen_'+MODEL_NAME+'.pb'

freeze_graph.freeze_graph(input_graph_path, input_saver="",
                          input_binary=False, input_checkpoint=checkpoint_path, 
                          output_node_names="y_", restore_op_name="save/restore_all",
                          filename_tensor_name="save/Const:0", 
                          output_graph=output_frozen_graph_name, clear_devices=True, initializer_nodes="")