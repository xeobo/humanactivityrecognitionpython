# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 20:25:40 2017

@author: Xeobo
"""
from enum import Enum
import threading
import time
from sklearn.preprocessing import StandardScaler
import pandas as pd

from keras.models import Sequential
from keras.models import model_from_json
import numpy as np


class AccelerometerData:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def getZ(self):
        return self.z

class IAccelerometerDataSource:
    def getCurrentReading():
        pass

class HumanActivityState(Enum):
    WALKING = 0
    RUNNING = 1
    STANDING = 2
    WALKING_UP = 3
    WALKING_DOWN = 4
    SITTING = 5
    IDLE = 6

WAIT_TIME = 1000/20 #20Hz = 50 ms
WINDOW_SIZE

class HumanActivityRecognizer():
    def __init__(self, callback, feedData):
        self.x_array_ = []
        self.y_array_ = []
        self.z_array_ = []
        
        self.callback = callback
        self.feedData = feedData
        
        self.state = HumanActivityState.IDLE
        
        self.inputThread_running = True
        self.lock = threading.Lock()
        self.inputThreadObject = threading.Thread(target = self.inputThread
                                              , args = (self))
        self.inputThreadObject.start()
        
        self.outputThread_running = True
        self.outputThreadObject = threading.Thread(target = self.outputThread
                                              , args = (self))
        self.outputThreadObject.start()
        
        #init mean normalisation
        dataset = pd.read_csv('har_scale_parameters.csv', names = ['mean','scale'], header = None)
        
        self.scX = StandardScaler()
        self.scX.mean_ = dataset['mean'][0]
        self.scX.scale_ = dataset['scale'][0]
        
        self.scY = StandardScaler()
        self.scY.mean_ = dataset['mean'][1]
        self.scY.scale_ = dataset['scale'][1]
        
        self.scZ = StandardScaler()
        self.scZ.mean_ = dataset['mean'][2]
        self.scZ.scale_ = dataset['scale'][2]
        
        #init keras model
        json_file = open('har_model_json', 'r')
        model_json =  json_file.read()
        json_file.close()
        self.model = model_from_json(model_json)
        # load weights into new model
        self.model.load_weights("har_model_save")
    
    def getCurrentState(self):
        return self.state
    
    def inputThread(self):
        while(self.inputThread_running):
            currentReading = self.feedData.getCurrentReading()
            
            currentReading = self.normalize(currentReading)
            
            self.lock.acquire()
            
            self.x_array_.extend(currentReading.getX())
            self.y_array_.extend(currentReading.getY())
            self.z_array_.extend(currentReading.getZ())
            
            self.lock.release()
            
            time.sleep(WAIT_TIME) 
    
    def outputThread(self):
        while(self.outputThread_running):
            if len(self.x_array_) == WINDOW_SIZE:
                self.lock.acquire()
                
                matrix = np.hstack(np.array(self.x_array_).transpose(),
                          np.array(self.y_array_).transpose(),
                          np.array(self.z_array_).transpose())
                
                del self.x_array_[0]
                del self.y_array_[0]                
                del self.z_array_[0]
                
                self.lock.release()
                
                self.state = model.predict(matrix).index(1)
                
            time.sleep(20)
            
    def normalize(self, currentReading):
        normalizedReading = AccelerometerData(
                self.scX.transform(currentReading.getX()),
                self.scX.transform(currentReading.getY()),
                self.scX.transform(currentReading.getZ()))
        return normalizedReading
        